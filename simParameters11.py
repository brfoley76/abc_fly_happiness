import pandas as pd
import numpy as np
from summaryStats import *

#set parameter values
#ranseed=605


# ABC 
bigJump=0.5 # proportion of the total prior range that a parameter might be mutated
littleJump=0.1 # proportion of the total prior range that a parameter might be mutated
bigLittleRatio=0.2
mutValues=0 #either use set initial parameters (1), or choose from the prior (0)
burnIn=2
storeInterval=2     #store the state at intervals of this many iterations
reportInterval=8   #report the results at intervals of this many iterations
runLength=999999999
output='Flexioutfile_11'
writeDiagnosticStats=1      #do we write out the value of each individual stat?

#### for flexible threshold simulations
flexiThreshold=1            # have an acceptance threshold that ratchets down slowly (1) or a fixed threshold (0)
Threshold=0                 # the fixed initial threshold if flexiThreshold==0, or the variable one if flexiThreshold==1
finalThreshold=-1           # a target threshold to begin fixed-threshold simulations and recording.
ratchetUp=0.05               # when in the initial wandering stage, allow the threshold to drift up again
numberOfSimsPerAttempt=5    #simulate up to this many times to assess fit
initThreshold=2500          #for picking some non-terrible initial starting position
searchThreshold=10
acceptanceRate=0.15 # if we stop accepting new proposals for too long, we're probably stuck, and need to go back to flexible mode again.


#Dict of ABC parameters
ABCParms={'bigJump': bigJump,'littleJump':littleJump,'bigLittleRatio':bigLittleRatio, 'mutValues':mutValues,'burnIn':burnIn,'output':output,'flexiThreshold':flexiThreshold, 'Threshold':Threshold, 'finalThreshold': finalThreshold,'ratchetUp':ratchetUp,'storeInterval':storeInterval, 'reportInterval':reportInterval, 'runLength':runLength, 'writeDiagnosticStats':writeDiagnosticStats,'numberOfSimsPerAttempt':numberOfSimsPerAttempt,
'acceptanceRate':acceptanceRate}

#simulation, fixed parameters
nReps=13 #this is the number output as 'The number of reps is 13' from calcEmpirical stats

nMales=1
nFemales=1
nPatches=2
nFood=1
FoodVal=1.0
Seconds=216000
nIntervals=100
maxMoves=107*3 #the most moves that are allowed in a simulation, 3x the max observed no, output from calcEmpiricalStats

#simulation, mutable parameters
fFoodPref=4.0
mFoodPref=3.0
fFemalePref=0.0 #this is gonna be null. Deleted from mutable parameters.
mFemalePref=0.8
fMalePref=-1.0
mMalePref=0.0 #this is gonna be null. Deleted from mutable parameters.
fInertia=10.0
mInertia=10.0
fLearn=2.0
mLearn=2.0

#Dict of all simulation parameters
Parms={'fFoodPref' : fFoodPref, 'mFoodPref':mFoodPref,'fFemalePref':fFemalePref,'mFemalePref':mFemalePref,
         'fMalePref':fMalePref,'mMalePref':mMalePref,'fInertia':fInertia,'mInertia':mInertia,
         'fLearn':fLearn,'mLearn':mLearn,'nFemales':nFemales,'nMales':nMales,'nPatches':nPatches,'nFood':nFood,
         'FoodVal':FoodVal,'Seconds':Seconds, 'nIntervals':nIntervals,'maxMoves':maxMoves, 'nReps':nReps}
#Dict of mutable parameters, and their priors
ParmPriors={'fFoodPref' : [-20,20,40], 'mFoodPref':[-20,20,40],'mFemalePref':[-20,20,40],
         'fMalePref':[-20,20,40],'fInertia':[0,15,15],'mInertia':[0,15,15],
         'fLearn':[0,20,20],'mLearn':[0,20,20]}

#Dicts of statistics, and their target values
moveStats={}
sampleStats={}
sampleStats['regPropFoodF']=[regPropFoodF,[0.787514884433,0.424611460987,-1.43661931499,1.46281056564]]
sampleStats['regPropFoodM']=[regPropFoodM,[0.113859816561,0.026172916044,0.243970700798,0.0979981800792]]
sampleStats['regMPerF']=[regMPerF,[-0.0162092637979,0.0358877498994,0.908857812199,0.330297039495]]

moveStats['regDuration']=[regDuration,[0.0995717806644,0.40712569582,3.06712502868,3.18224842118]]
moveStats['regMovesF']=[regMovesF,[-0.0409538137498,0.163416357553,0.558328173485,0.815298344813]]
moveStats['regFLikLeaveFood']=[regFLikLeaveFood,[0.614798964248,4.63375033978,-1.66500492814,4.28592202021]]
moveStats['LikFoodF']=[LikFoodF,[1.06519071646,0.850426062203]]
moveStats['nMovesM']=[nMovesM,[44.4615384615,32.2719582601]]
moveStats['LikFoodM']=[LikFoodM,[1.11816934074,0.775355721591]]
moveStats['meanDurationSub1']=[meanDurationSub1,[177.10176547,176.351763332]]
moveStats['mPerF']=[mPerF,[0.824385327635,0.157864727533]]
moveStats['regMLikLeaveFood']=[regMLikLeaveFood,[-1.05606714431,4.25049088859,2.48856141572,2.00823342086]]
moveStats['regFLikLeaveM']=[regFLikLeaveM,[3.22115932725,2.79784927614]]
moveStats['regMLikLeaveF']=[regMLikLeaveF,[-2.11264702568,2.58125270264]]
moveStats['nMovesF']=[nMovesF,[33.6153846154,26.5432548625]]

#dicts are unordered, so I'll make an ordered vector for output: df = pd.DataFrame(data, columns=data.keys())
#df.to_csv('my_csv.csv', mode='a', header=False)
#These have extra parameters, not in the dicts themselves.
orderedOutColumns=['Iteration','Accepts','Threshold','AcceptanceRate','fFoodPref','mFoodPref','mFemalePref','fMalePref','fInertia','mInertia','fLearn','mLearn']


StatVecOut=['regPropFoodF_1','regPropFoodF_2','regPropFoodM_1','regPropFoodM_2',
'regMPerF_1','regMPerF_2','meanDurationSub1_1','regDuration_1',
'regDuration_2','regMovesF_1','regMovesF_2',
'LikFoodF_1','LikFoodM_1','mPerF_1','nMovesF_1','nMovesM_1',
'regFLikLeaveFood_1','regFLikLeaveFood_2','regMLikLeaveFood_1','regMLikLeaveFood_2',
'regFLikLeaveM_1','regMLikLeaveF_1']


'regFLikLeaveFood_2': [0.97964518826861224, 1.9473871352211762, 2.1264594683660283, 2.1513560980748943], 

'regDuration_2': [0.42727143859438022, 0.4559377186012718, 0.3281583452148078, 0.36385956134593084], 

'regDuration_1': [-0.31063373061881505, -0.363421957543341, -0.32334289343228828, -0.32521507545695694], 

'regFLikLeaveFood_1': [-2.7084979538147724, -1.3213120991716603, -1.3579786295219614, -1.5492620713259355], 

'regPropFoodF_2': [-0.9852359646234331, -0.90389458482054919, -0.4831474778560863, -0.53088468215600426], 

'regPropFoodF_1': [1.1177905357595996, -1.5277924104411438, -1.6270199072853277, -1.352983874285429], 

'regPropFoodM_1': [-4.0645547130811659, -0.096342075778630382, 0.084135775067200699, -0.50046364261015674], 

'regMLikLeaveFood_1': [-1.5560337086768956, -1.3490116854889893, -0.94153926911908603, -1.0388555784492597], 

'regPropFoodM_2': [-1.9453353273044942, -0.37335460883715688, -0.091875610673088401, -0.08036504208294365], 

'regMLikLeaveFood_2': [0.92590960369151776, -1.4952487349157413, -1.6517011586188965, -1.5013648316353245], 

'LikFoodM_1': [-1.1213464368009047, 1.3125009444644737, -0.1249840827927155, 0.26999663559922965], 

'regMLikLeaveF_1': [3.9947610917018932, 2.9673099726287453, 1.1530266223512844, 2.6569355845818148], 

'nMovesM_1': [8.5999882406156747, 4.464461744643911, 4.2284864575538794, 4.3905704931308716], 

'regMPerF_1': [-3.0839524353536878, -3.0110739790664498, -3.0912577700433186, -3.1768439252358465],

'regMPerF_2': [1.0961922033440676, 1.0810374381469339, 0.840341854499708, 0.86306305443903841], 

'LikFoodF_1': [5.2090639658578262, -2.2846184273966617, -2.8998784305769387, -1.7426878106173842], 

'nMovesF_1': [-1.2287635704194904, 3.7993137755163833, 4.0862184771000454, 3.8891526214668226], 

'regFLikLeaveM_1': [-3.1150573200025002, -2.1925970199643965, -1.8002680297525357, -2.6974731284687077], 

'regMLikLeaveM_1': [], 

'regMovesF_1': [-3.5411499781113069, 0.32629054044700012, 0.33762008071047511, 0.2500638738354895], 

'regMovesF_2': [-0.40901417458456107, -1.2588614581574311, -1.154540616343114, -1.0562818776536338], 

'mPerF_1': [-1.6734557326083757, -2.9079813231035554, -1.2930290828685176, -1.168636639445966], 

'meanDurationSub1_1': [0.7073846555864558, 1.449006756085784, 0.47263621404338702, 0.39750081372558521]}

