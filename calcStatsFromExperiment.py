import numpy as np
import pandas as pd
import math
import scipy as sp
import copy
import sys
import commands
import os
import subprocess
import re
import fileinput
import random
from sklearn import linear_model as lm # for regression
import scipy.stats as st # for getting the exponential distribution
from summaryStats import *

# two general categories of stats. Those that use the dfMoves dataset, and those that use the SampleRecord 
moveStats={}
sampleStats={}
#sampleStats['regPropFoodF']=[regPropFoodF,[0,1,0,1]]# regression of log likelihood female-being-on-food, with logtime
sampleStats['regPropFoodM']=[regPropFoodM,[0,1,0,1]] # regression of log likelihood male-being-on-food, with logtime
#sampleStats['regMPerF']=[regMPerF,[0,1,0,1]] # regression of males-per-female w time
sampleStats['regMPerM']=[regMPerM,[0,1,0,1]] # correlation of males-per-male w time

moveStats['meanDurationSub1']=[meanDurationSub1,[0,1]]
#moveStats['LikFoodF']=[LikFoodF,[0,1]]
moveStats['LikFoodM']=[LikFoodM,[0,1]] #  mean proportion of (log?) time males spend on food
#moveStats['mPerF']=[mPerF,[0,1]] # mean number of males-per-female is perfectly correlated with fPerM
moveStats['mPerM']=[mPerM,[0,1]] # mean number of males-per-male
#moveStats['nMovesF']=[nMovesF,[0,1]]# number of female moves
moveStats['nMovesM']=[nMovesM,[0,1]] # number of male moves
#moveStats['regMovesF']=[regMovesF,[0,1,0,1]] # regression of proportion of female moves over time
moveStats['regDuration']=[regDuration,[0,1,0,1]] # change in durations over time
#moveStats['regFLikLeaveM']=[regFLikLeaveM,[0,1]] # likelihood of a female leaving, per male present
#moveStats['regMLikLeaveF']=[regMLikLeaveF,[0,1]] #  likelihood of a male leaving, per female present
moveStats['regMLikLeaveM']=[regMLikLeaveM,[0,1]] # likelihood of a male leaving, per male present
#moveStats['regFLikLeaveFood']=[regFLikLeaveFood,[0,1,0,1]] # likelihood of a female leaving, given food
moveStats['regMLikLeaveFood']=[regMLikLeaveFood,[0,1,0,1]] #  likelihood of a male leaving, given food


pathMoves="../data_for_stats/FlyMindMoves.csv"
pathSample="../data_for_stats/FlyStateSample.csv"
expState="02"


def CalcEmpiricalStats(pathMoves, pathSample, moveStats, sampleStats, expState):
    dfAllMoves=pd.read_csv(pathMoves, dtype=dict([('expState',object), ('Duration',float)]))
    dfAllSamps=pd.read_csv(pathSample)
    uniqueMoves=np.unique(dfAllMoves[(dfAllMoves.expState == expState)].Exp)
    print 'The number of reps is ' + str(len(uniqueMoves))
    sampOut={}
    moveOut={}
    appendix=["_1", "_2"]
    for stat in moveStats:
        nMeasures=len(moveStats[stat][1])/2
        moveOut[stat]=[]
        for i in range(0,nMeasures):
            app=stat+appendix[i]
            aaa=moveOut[stat]
            aaa.append([app])
    for stat in sampleStats:
        nMeasures=len(sampleStats[stat][1])/2
        sampOut[stat]=[]
        for i in range(0,nMeasures):
            app=stat+appendix[i]
            sampOut[stat].append([app])
    maxMoves=[]          
    for exp in uniqueMoves:
        dfMoves=dfAllMoves[dfAllMoves.Exp==exp]
        nMoves=max(dfMoves.Moves)
        maxMoves.append(nMoves)
        SampleRecord=dfAllSamps[dfAllSamps.Exp==exp]
        for fun in sampleStats:
            Do=sampleStats[fun]
            output=Do[0](SampleRecord, Do[1])
            for i in range(0, len(output)):
                sampOut[fun][i].append(output[i])
        for fun in moveStats:
            Do=moveStats[fun]
            output=Do[0](dfMoves,Do[1])
            for i in range(0, len(output)):
                moveOut[fun][i].append(output[i])
    print 'The most moving experience was '+str(max(maxMoves))            
    return sampOut, moveOut

def writeTargets(sampOut, moveOut):
    f = open('target02_out.py', 'w')
    f.write('moveStats={}\n',)
    f.write('sampleStats={}\n')
    for stat in sampOut:
        f.write('sampleStats[\''+ stat+'\']=['+stat+',[')
        for i in range(0, len(sampOut[stat])):
            myMean=np.mean(sampOut[stat][i][1:len(sampOut[stat][i])]) 
            myStd=np.std(sampOut[stat][i][1:len(sampOut[stat][i])])
            if i>0:
                f.write(',')
            f.write(str(myMean) +','+ str(myStd))            
        f.write(']]\n')               
    for stat in moveOut:
        f.write('moveStats[\''+ stat+'\']=['+stat+',[')
        for i in range(0, len(moveOut[stat])):
            myMean=np.mean(moveOut[stat][i][1:len(moveOut[stat][i])]) 
            myStd=np.std(moveOut[stat][i][1:len(moveOut[stat][i])])
            if i>0:
                f.write(',')
            f.write(str(myMean) +','+ str(myStd))            
        f.write(']]\n')               
    f.close()  

sampOut, moveOut=CalcEmpiricalStats(pathMoves, pathSample, moveStats, sampleStats, expState)
writeTargets(sampOut, moveOut)
