This is a collection of scripts that run an ABC analysis using a simulation of fly behaviour to describe a dataset comprising joining and leaving decisions, across time, for flies in a two patch assay. One patch has food, the other doesn't. The manipulation I am focussing on for development of the scripts is for the case where there is two males, and one female, in the experiment.

TO RUN SINGLE JOB:

```
$ python ABCshell.py
```
    
# Files

*  `ABCshell.py` : imports the parameters simParameters[12\\11\\02].py, the simulation code simulation\_functions.py, and the summary statistics summaryStats.py. In the function self.runARep() it runs the ABC analysis by calling Replicate() (imported from simulation\_functions.py) with the simulation parameters self.Parms, a couple of dictionaries for recording stats (and in the debugging process a few other random variables). It then uses Replicate.fit\_stat as the statistic to evaluate parameter combinations. For now, edit line 24 to set the ranseed, and line 22 to edit which set of simulations is to be run.
    
*  `simulation\_functions.py` : Creates a class 'Replicate' that takes three arguments 
    
    * Parms=a dict of parameters imported from simParameters12, and modified by ABCshell;  
    
    * moveStats=a dict to record all the events observed in the simulation;
    
    * sampleStats= a similar dict, that samples the states at evenly spaced intervals.

    It produces a couple simulation diagnostics. 
    
    * self.fit_stats is a single number describing fit
    
    * self.statOutDict is a record of how individual statistics fit the simulated data.
        
*    `simParameters[12\11\02].py` : initialises all the parameters, lists and dictionaries the analysis needs to run

*   `bash_abc_sim_submission.py` : creates a range of ranseeds, and for each one edits the ABCshell file to change the ranseed and output file, writes a bash script, and submits it using qsub

# Simulation

*   The simulation models a variable number of male `nMales`, and female `nFemales`, flies as they move among `nPatches` patches. `nFood` of these patches have food of nominal value `FoodVal`. All the current simulations only have 

