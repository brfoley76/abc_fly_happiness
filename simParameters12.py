import pandas as pd
import numpy as np
from summaryStats import *

#set parameter values
#ranseed=605

# ABC 
bigJump=0.5 # proportion of the total prior range that a parameter might be mutated
littleJump=0.1 # proportion of the total prior range that a parameter might be mutated
bigLittleRatio=0.2
mutValues=0 #either use set initial parameters (1), or choose from the prior (0)
burnIn=100
storeInterval=5 #store the state at intervals of this many iterations
reportInterval=25 #report the results at intervals of this many iterations
runLength=999999999         #nominally large
output='Flexioutfile_12'         #the prefix for the outfile name
writeDiagnosticStats=1      #do we write out the value of each individ

#### for flexible threshold simulations
flexiThreshold=1 # have an acceptance threshold that ratchets down slowly (1) or a fixed threshold (0)
Threshold=0 # the fixed initial threshold if flexiThreshold==0, or the variable one if flexiThreshold==1
finalThreshold=-1 # a target threshold to begin fixed-threshold simulations and recording.
ratchetUp=0.2 # when in the initial wandering stage, allow the threshold to drift up again
numberOfSimsPerAttempt=5 #simulate up to this many times to assess fit
initThreshold=500 
searchThreshold=30 #stop randomly varying the starting position when it drops below this
acceptanceRate=0.15 # if we stop accepting new proposals for too long, we're probably stuck, and need to go back to flexible mode again.

#Dict of ABC parameters
ABCParms={'bigJump': bigJump,'littleJump':littleJump,'bigLittleRatio':bigLittleRatio, 'mutValues':mutValues,'burnIn':burnIn,'output':output,'flexiThreshold':flexiThreshold, 'Threshold':Threshold, 'finalThreshold': finalThreshold,'ratchetUp':ratchetUp,'storeInterval':storeInterval, 'reportInterval':reportInterval, 'runLength':runLength, 'writeDiagnosticStats':writeDiagnosticStats,'numberOfSimsPerAttempt':numberOfSimsPerAttempt,
'acceptanceRate':acceptanceRate}

#simulation, fixed parameters
nReps=8 
nMales=2
nFemales=1
nPatches=2
nFood=1
FoodVal=1.0
Seconds=216000
nIntervals=100
maxMoves=160*3 #the most moves that are allowed in a simulation, 3x the max observed no.

#simulation, mutable parameters
fFoodPref=4.0
mFoodPref=3.0
fFemalePref=0.0 #this is gonna be null. Deleted from mutable parameters.
mFemalePref=0.8
fMalePref=-1.0
mMalePref=-1.0
fInertia=10.0
mInertia=10.0
fLearn=2.0
mLearn=2.0

#Dict of all simulation parameters
Parms={'fFoodPref' : fFoodPref, 'mFoodPref':mFoodPref,'fFemalePref':fFemalePref,'mFemalePref':mFemalePref,
         'fMalePref':fMalePref,'mMalePref':mMalePref,'fInertia':fInertia,'mInertia':mInertia,
         'fLearn':fLearn,'mLearn':mLearn,'nFemales':nFemales,'nMales':nMales,'nPatches':nPatches,'nFood':nFood,
         'FoodVal':FoodVal,'Seconds':Seconds, 'nIntervals':nIntervals,'maxMoves':maxMoves, 'nReps':nReps}
#Dict of mutable parameters, and their priors
ParmPriors={'fFoodPref' : [-20,20,40], 'mFoodPref':[-20,20,40],'mFemalePref':[-20,20,40],
         'fMalePref':[-20,20,40],'mMalePref':[-20,20,40],'fInertia':[0,15,15],'mInertia':[0,15,15],
         'fLearn':[0,20,20],'mLearn':[0,20,20]}

#Dicts of statistics, and their target values
moveStats={}
sampleStats={}
sampleStats['regPropFoodF']=[regPropFoodF,[0.715386390013,0.333273069019,-1.1081592895,1.41505389382]]
sampleStats['regPropFoodM']=[regPropFoodM,[0.20082285369,0.064169473362,0.41503379135,0.17574761209]]
sampleStats['regMPerF']=[regMPerF,[-0.0471232799052,0.141324176575,1.68386366123,0.81455005403]]
sampleStats['regMPerM']=[regMPerM,[-0.418729492007,0.284080795435,2.08079998703,0.644005611614]]

moveStats['regDuration']=[regDuration,[-0.104281310195,0.168748191844,3.70797538254,1.34761375734]]
moveStats['regMovesF']=[regMovesF,[-0.225262864151,0.183058130833,1.13369323043,1.17845263408]]
moveStats['regFLikLeaveFood']=[regFLikLeaveFood,[-0.648888468113,1.13316631252,-1.34774226544,1.0222819034]]
moveStats['LikFoodF']=[LikFoodF,[1.00224858437,0.579816976525]]
moveStats['LikFoodM']=[LikFoodM,[0.885525098985,0.782184512048]]
moveStats['meanDurationSub1']=[meanDurationSub1,[35.2247711499,11.2837696534]]
moveStats['regMLikLeaveM']=[regMLikLeaveM,[0.355796262304,0.40645483686]]
moveStats['mPerF']=[mPerF,[1.45096238426,0.173145981592]]
moveStats['nMovesM']=[nMovesM,[198.75,44.9353702555]]
moveStats['regFLikLeaveM']=[regFLikLeaveM,[1.00504890176,0.592054391172]]
moveStats['regMLikLeaveF']=[regMLikLeaveF,[-0.445695896731,0.468487434799]]
moveStats['regMLikLeaveFood']=[regMLikLeaveFood,[-0.348258881394,0.710673555958,0.0941573876465,0.406222977628]]
moveStats['nMovesF']=[nMovesF,[82.125,39.3682533903]]
moveStats['mPerM']=[mPerM,[0.688858217593,0.118847017732]]

#dicts are unordered, so I'll make an ordered vector for output: df = pd.DataFrame(data, columns=data.keys())
#df.to_csv('my_csv.csv', mode='a', header=False)
#These have extra parameters, not in the dicts themselves.
orderedOutColumns=['Iteration','Accepts','Threshold','AcceptanceRate','fFoodPref','mFoodPref','mFemalePref','fMalePref','mMalePref','fInertia','mInertia','fLearn','mLearn']


StatVecOut=['regPropFoodF_1','regPropFoodF_2','regPropFoodM_1','regPropFoodM_2',
'regMPerF_1','regMPerF_2','regMPerM_1','regMPerM_2','meanDurationSub1_1','regDuration_1',
'regDuration_2','regMovesF_1','regMovesF_2','LikFoodF_1','LikFoodM_1','mPerF_1','mPerM_1',
'nMovesF_1','nMovesM_1','regFLikLeaveFood_1','regFLikLeaveFood_2',
'regMLikLeaveFood_1','regMLikLeaveFood_2','regFLikLeaveM_1','regMLikLeaveF_1','regMLikLeaveM_1']






