import numpy as np
from sklearn import linear_model as lm # for regression
import scipy.stats as st # for getting the exponential distribution

def regMPerM(SampleRecord, standards):
    # regression coeff. of males-per-female with time
    # we can do this as logistic regression since mPerM never goes above 1
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            log_model=lm.LogisticRegression()
            y=(SampleRecord.p0M*(SampleRecord.p0M-1)+SampleRecord.p1M*(SampleRecord.p1M-1))
            y=y/2 #make it binomial, so we can do logistic regression
            X=SampleRecord.IntEnd.reshape(-1,1)
            X=X-4 #center the x axis, to reduce correlation between slope and intercept
            mPerMmod=log_model.fit(X,y)
            mPerMslope=mPerMmod.coef_[0][0]
            mPerMInt=mPerMmod.intercept_[0]
        except:
            mPerMslope,mPerMInt=11,11
        if np.isnan(mPerMslope):
            mPerMslope=11
            mPerMInt=11
    else:
        mPerMslope=12
        mPerMInt=12
    return [mPerMslope, mPerMInt]


def regMPerF(SampleRecord, standards):
    # regression coeff. of males-per-female with time
    # has to be linear, (or Poisson I guess?) since mPerF can be 0, 1, or 2
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            mPerF=SampleRecord.p0F*SampleRecord.p0M+SampleRecord.p1F*SampleRecord.p1M
            intEnd=SampleRecord.IntEnd-4 #center the x axis, to reduce correlation between slope and intercept
            mfMod=st.linregress(SampleRecord.IntEnd,mPerF)
            mfSlope=mfMod.slope 
            mfIntercept=mfMod.intercept
            mfSlope=(mfSlope-meanSlope)/sdSlope
            mfIntercept=(mfIntercept-meanIntercept)/sdIntercept
        except:
            mfSlope,mfIntercept=11,11
        if np.isnan(mfSlope):
            mfSlope=11
            mfIntercept=11
    else:
        mfSlope=12
        mfIntercept=12
    return [mfSlope, mfIntercept]


def regPropFoodF(SampleRecord, standards):
    # regression of female-being-on-food, with logtime
    # Logistic.
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            log_model=lm.LogisticRegression()
            y=SampleRecord.p1F.values
            X=SampleRecord.IntEnd.reshape(-1,1)
            X=X-4 #center the x axis, to reduce correlation between slope and intercept
            propFoodFmod=log_model.fit(X,y)
            foodSlopeF=propFoodFmod.coef_[0][0]
            foodInterceptF=propFoodFmod.intercept_[0]
            foodSlopeF=(foodSlopeF-meanSlope)/sdSlope
            foodInterceptF=(foodInterceptF-meanIntercept)/sdIntercept
        except:    
            foodSlopeF,foodInterceptF=11,11
        if np.isnan(foodSlopeF):
            foodSlopeF=11
            foodInterceptF=11
    else:
        foodSlopeF=12
        foodInterceptF=12
    return [foodSlopeF, foodInterceptF]


def regPropFoodM(SampleRecord, standards):
    # regression of male-being-on-food, with logtime
    # Logistic, with weights
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            y=SampleRecord.p1M.values
            X=SampleRecord.IntEnd-4 #center the x axis, to reduce correlation between slope and intercept
            foodModM=st.linregress(X,y)
            foodSlopeM=foodModM.slope
            foodInterceptM=foodModM.intercept
            foodSlopeM=(foodSlopeM-meanSlope)/sdSlope
            foodInterceptM=(foodInterceptM-meanIntercept)/sdIntercept
        except:
            foodSlopeM,foodInterceptM=11,11
        if np.isnan(foodSlopeM):
            foodSlopeM=11
            foodInterceptM=11
    else:
        foodSlopeM=12
        foodInterceptM=12
    return [foodSlopeM, foodInterceptM]


def meanDurationSub1(dfMoves, standards):
    #besides the last interval (tacked on, incomplete), how long on average are the intervals? 
    #print 'meanDurationSub1'
    mean=standards[0]
    sd=standards[1]
    singleFly=dfMoves.iloc[0,:].FlyId
    try:
        durVec=dfMoves[dfMoves['FlyId']==singleFly].Duration
        durVec=durVec.iloc[range(0, (len(durVec)-1))]
        durMean=np.mean(durVec)
        durMean=(durMean-mean)/sd
    except:
        durMean=11
    return [durMean]


def LikFoodF(dfMoves, standards):
    # mean proportion of time females spend on food
    #print 'LikFoodF'
    mean=standards[0]
    sd=standards[1]
    try:
        On=np.sum(dfMoves[(dfMoves['Sex']==0) & (dfMoves['Food']==1)].Duration)+1
        Off=np.sum(dfMoves[(dfMoves['Sex']==0) & (dfMoves['Food']==0)].Duration)+1
        fLikeFood=np.log(On/Off)
        fLikeFood=(fLikeFood-mean)/sd
    except:
        fLikeFood=11
    if (np.isinf(fLikeFood) or np.isneginf(fLikeFood)):
        fLikeFood=11
    return [fLikeFood]

def LikFoodM(dfMoves, standards):
    # mean proportion of (log?) time females spend on food
    #print 'LikFoodM'
    mean=standards[0]
    sd=standards[1]
    try:
        On=np.sum(dfMoves[(dfMoves['Sex']==1) & (dfMoves['Food']==1)].Duration)+1
        Off=np.sum(dfMoves[(dfMoves['Sex']==1) & (dfMoves['Food']==0)].Duration)+1
        mLikeFood=np.log(On/Off)
        mLikeFood=(mLikeFood-mean)/sd
    except:
        mLikeFood=11
    if (np.isinf(mLikeFood) or np.isneginf(mLikeFood)):
        mLikeFood=11
    return [mLikeFood]
    

def mPerF(dfMoves, standards):
    # mean number of males-per-female
    #print 'mPerF'
    mean=standards[0]
    sd=standards[1]
    try:
        nM=np.sum((dfMoves[dfMoves['Sex']==0].nMales)*(dfMoves[dfMoves['Sex']==0].Duration))/np.sum(dfMoves[dfMoves['Sex']==0].Duration)
        nM=(nM-mean)/sd
    except:
        nM=11
    if (np.isinf(nM) or np.isneginf(nM)):
        nM=11        
    return [nM]


def mPerM(dfMoves, standards):
    # mean number of males-per-female
    #print 'mPerM'
    mean=standards[0]
    sd=standards[1]
    try:
        nM=np.sum((dfMoves[dfMoves['Sex']==1].nMales)*(dfMoves[dfMoves['Sex']==1].Duration))/np.sum(dfMoves[dfMoves['Sex']==1].Duration) #note nMales already corrected for focal male
        nM=(nM-mean)/sd
    except:
        nM=11
    if (np.isinf(nM) or np.isneginf(nM)):
        nM=11        
    return [nM]


def nMovesF(dfMoves, standards):
    #count of female moves
    #print 'nMovesF'
    mean=standards[0]
    sd=standards[1]
    try:
        moves=np.sum(dfMoves[dfMoves['Sex']==0].Leave)
        moves=(moves-mean)/sd
    except:
        moves=11
    if (np.isinf(moves) or np.isneginf(moves)):
        moves=11                
    return [moves]


def nMovesM(dfMoves,standards):
    #count of male moves
    #print 'nMovesM'
    mean=standards[0]
    sd=standards[1]
    try:
        moves=np.sum(dfMoves[dfMoves['Sex']==1].Leave)
        moves=(moves-mean)/sd
    except:
        moves=11
    if (np.isinf(moves) or np.isneginf(moves)):
        moves=11
    return [moves]


def regMovesF(dfMoves, standards):
    #regression of whether-moves-were-made-by-females or not, with time
    #print 'regMovesF'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]    
    try:    
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==0),('logTime')].reshape(-1,1)
        y=dfMoves.loc[(dfMoves['Sex']==0),('Leave')]
        fMovesMod=log_model.fit(X,y)
        fMovesSlope=fMovesMod.coef_[0][0]
        fMovesSlope=(fMovesSlope-meanSlope)/sdSlope
        fMovesIntercept=fMovesMod.intercept_[0]
        fMovesIntercept=(fMovesIntercept-meanIntercept)/sdIntercept
    except:
        fMovesSlope,fMovesIntercept=11,11
    if np.isnan(fMovesSlope):
        fMovesSlope=11
        fMovesIntercept=11       
    return [fMovesSlope, fMovesIntercept]


def regDuration(dfMoves, standards):
    #change in duration lengths with time
    #needs to be on a log-log scale to make residuals normal
    meanSlope=standards[0]
    sdSlope=standards[1]
    try:
        singleFly=dfMoves.iloc[0,:].FlyId
        meanIntercept=standards[2]
        sdIntercept=standards[3]    
        y=dfMoves[dfMoves['FlyId']==singleFly].Duration
        yLen=len(y)-1
        y=y[0:yLen]#remove truncated duration
        y=np.log(y+1)
        X=dfMoves[dfMoves['FlyId']==singleFly].logTime
        X=X[0:yLen]
        durationMod=st.linregress(X,y)
        durSlope=durationMod.slope
        durIntercept=durationMod.intercept
        durSlope=(durSlope-meanSlope)/sdSlope
        durIntercept=(durIntercept-meanIntercept)/sdIntercept
    except:
        durSlope,durIntercept=11,11
    if np.isnan(durSlope):
        durSlope=11
        durIntercept=11               
    return [durSlope, durIntercept]


def regMLikLeaveM(dfMoves, standards):
    #change in male leaving likelihood with n males present
    #print 'regMLikLeaveM'
    meanSlope=standards[0]
    sdSlope=standards[1]
    #meanIntercept=standards[2] #for the regression stats, only one per sex should report the intercept.
    #sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==1),['nMales','nFemales','Food']]
        sample_weight=dfMoves.loc[(dfMoves['Sex']==1),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==1),('Leave')]
        mLeaveMmod=log_model.fit(X,y, sample_weight=sample_weight)
        mLeaveMslope=mLeaveMmod.coef_[0][0]
        mLeaveMslope=(mLeaveMslope-meanSlope)/sdSlope
        #mLeaveMint=mLeaveMmod.intercept_[0]
        #mLeaveMint=(mLeaveMint-meanIntercept)/sdIntercept
    except:
        mLeaveMslope=11
    if np.isnan(mLeaveMslope):
        mLeaveMslope=11
        #mLeaveMint=11                 
    return [mLeaveMslope]


def regMLikLeaveF(dfMoves,standards):
    #change in male leaving likelihood with n females present
    #print 'regMLikLeaveF'
    meanSlope=standards[0]
    sdSlope=standards[1]
    #meanIntercept=standards[2] #for the regression stats, only one per sex should report the intercept.
    #sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==1),['nFemales','nMales','Food']]
        sample_weight=dfMoves.loc[(dfMoves['Sex']==1),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==1),('Leave')]
        mLeaveFmod=log_model.fit(X,y, sample_weight=sample_weight)
        mLeaveFslope=mLeaveFmod.coef_[0][0]
        mLeaveFslope=(mLeaveFslope-meanSlope)/sdSlope
        #mLeaveFint=mLeaveFmod.intercept_[0]
        #mLeaveFint=(mLeaveFint-meanIntercept)/sdIntercept    
    except:
        mLeaveFslope=11
    if np.isnan(mLeaveFslope):
        mLeaveFslope=11
        #mLeaveFint=11                       
    return [mLeaveFslope]


def regFLikLeaveM(dfMoves, standards):   
    #change in female leaving likelihood with n males present
    meanSlope=standards[0]
    sdSlope=standards[1]
    #meanIntercept=standards[2] #for the regression stats, only one per sex should report the intercept.
    #sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==0),['nMales','Food']]
        sample_weight=dfMoves.loc[(dfMoves['Sex']==0),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==0),('Leave')]
        fLeaveMmod=log_model.fit(X,y, sample_weight=sample_weight)
        fLeaveMslope=fLeaveMmod.coef_[0][0]
        fLeaveMslope=(fLeaveMslope-meanSlope)/sdSlope
        #fLeaveMint=fLeaveMmod.intercept_[0]
        #fLeaveMint=(fLeaveMint-meanIntercept)/sdIntercept    
    except:
        fLeaveMslope,fLeaveMint=11,11
    if np.isnan(fLeaveMslope):
        mLeaveFslope=11
        fLeaveMint=11                               
    return [fLeaveMslope]
    
    
def regMLikLeaveFood(dfMoves, standards):
    #change in male leaving likelihood with food present
    #print 'regMLikLeaveM'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==1),['Food','nMales','nFemales']]
        sample_weight=dfMoves.loc[(dfMoves['Sex']==1),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==1),('Leave')]
        mLeaveMmod=log_model.fit(X,y, sample_weight=sample_weight)
        mLeaveMslope=mLeaveMmod.coef_[0][0]
        mLeaveMslope=(mLeaveMslope-meanSlope)/sdSlope
        mLeaveMint=mLeaveMmod.intercept_[0]
        mLeaveMint=(mLeaveMint-meanIntercept)/sdIntercept
    except:
        mLeaveMslope,mLeaveMint=11,11
    if np.isnan(mLeaveMslope):
        mLeaveMslope=11
        mLeaveMint=11                 
    return [mLeaveMslope, mLeaveMint]


def regFLikLeaveFood(dfMoves, standards):   
    #change in female leaving likelihood with food present
    #print 'regFLikLeaveM'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==0),['Food','nMales']]
        sample_weight=dfMoves.loc[(dfMoves['Sex']==0),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==0),('Leave')]
        fLeaveMmod=log_model.fit(X,y, sample_weight=sample_weight)
        fLeaveMslope=fLeaveMmod.coef_[0][0]
        fLeaveMslope=(fLeaveMslope-meanSlope)/sdSlope
        fLeaveMint=fLeaveMmod.intercept_[0]
        fLeaveMint=(fLeaveMint-meanIntercept)/sdIntercept    
    except:
        fLeaveMslope,fLeaveMint=11,11
    if np.isnan(fLeaveMslope):
        mLeaveFslope=11
        fLeaveMint=11                               
    return [fLeaveMslope, fLeaveMint]

