import random
import profile
import numpy as np
import pandas as pd
import copy 
nInts=100
Length=216000/float(nInts)
nameVec=["p0F","p0M","p1F","p1M"]

class Patch:
    def __init__(self):
        self.nF=random.random()
        self.nM=random.random()
        
patches={}
patches[0]=Patch()
patches[1]=Patch()

def indexAdd(SampleRecord, patches, tick):
    for patch in patches:
        #print patch
        slot=int(patch)*2
        SampleRecord.iloc[tick, slot]=patches[patch].nF
        SampleRecord.iloc[tick, (slot+1)]=patches[patch].nM
        patches[patch].nF=patches[patch].nF+0.1
        patches[patch].nM=patches[patch].nM+0.1
        return SampleRecord

def currentMethod(patches, nameVec, nInts, Length):
    IntEnd=range(1,nInts)
    tocker=range(0,(nInts-1))
    for i in range(0,len(IntEnd)):
        intervalTick=float(IntEnd[i])/float(nInts)
        IntEnd[i]=np.log(intervalTick*(Length+1))
    Index=range(0,(nInts-1))
    data = [pd.Series([0 for j in Index],
        index=[Index],
        name=i)  for i in nameVec]
    SampleRecord = pd.concat(data, axis=1)
    SampleRecord['IntEnd']=pd.Series(IntEnd, index=SampleRecord.index)
    for tick in tocker:
        #print tick
        SampleRecord=indexAdd(SampleRecord, patches, tick)
        
def repeatCurrent(patches, nameVec, nInts, Length):
    for x in range(0,10):
        #print x
        currentMethod(patches, nameVec, nInts, Length)
        
currentMethod(patches, nameVec, nInts, Length)
#aaa=profile.run('repeatCurrent(patches, nameVec, nInts, Length)')
#aaa.print_stats(sort='time')
#repeatCurrent(patches, nameVec, nInts, Length)
