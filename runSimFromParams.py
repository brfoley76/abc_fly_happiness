import numpy as np
import pandas as pd
import math
import scipy as sp
import copy
import sys
import commands
import random
import scipy.stats as st # for getting the exponential distribution
from summaryStats import *

class generateData():
    
    def __init__(self):
        import simParametersManual as par
        import simulation_functions as sim
        self.Parms=par.Parms
        self.moveStats=par.moveStats #dict of target stats1 for simulation
        self.sampleStats=par.sampleStats #dict of target stats 2 for simulation        
        #initialise the dicts containing the sim outcomes
       
        self.StatVecOut=par.StatVecOut
        self.outStatRecord={}
        for key in self.StatVecOut:
            self.outStatRecord[key]=[] #will keep all the running stats outputs
        #ordered list of the mutable parameters for output
        self.orderedOutColumns=par.orderedOutColumns
        self.paramStateRecord={}
        
        simRep=sim.Replicate(self.Parms, self.moveStats,self.sampleStats, Iteration=0, myRanseed=0)
        self.dfMoves=simRep.dfMoves
        self.SampleRecord=simRep.SampleRecord
        
    def calcStats(self):
        pass        

