import sys
import commands
import os
import subprocess
import re
import fileinput

####This script opens up my statistics file, reads each line at a time
##### and creates a new config.xml file. It then submits flysim to the cluster, using the new config file.

#def SubmitTheJob(geno, dens):


def replaceLine(i,paramsName,lineNum):
    infile=paramsName+'.py'
    outfile=paramsName+'_'+str(i)+'.py'
    lines = open(infile, 'r').readlines()
    lines[lineNum]=lines[lineNum].replace('999', str(i))
    print lines[lineNum]
    out = open(outfile, 'w')
    out.writelines(lines)
    out.close()


def SubmitTheJob(i,paramsName, lineNum):
    replaceLine(i,paramsName,lineNum)
    MakeTheScript(i)
    BashTheScript(i)

    
def MakeTheScript(i):
    #the following should ensure scr.i is empty
    MyDummyName='submit.'+str(i)+'.pbs'
    jobName='brad_ABC.'+str(i)
    #print 'jobname is ' + jobName
    MyDummy=open(MyDummyName, 'w')
    MyDummy.write('#!/bin/bash\n')
    MyDummy.write('#PBS -l nodes=1:ppn=1\n')
    MyDummy.write('#PBS -N '+jobName+'\n')
    MyDummy.write('#PBS -l walltime=72:00:00\n') 
    MyDummy.write('#PBS -l mem=500mb\n')    
    MyDummy.write('#PBS -l pmem=500mb\n')
    MyDummy.write('#PBS -l vmem=500mb\n')            
    MyDummy.write('[[ $(ulimit -s) > 10240 ]] && ulimit -s 10240\n')
    MyDummy.write('cd /panfs/cmb-panasas2/bradfole/ABC_twopatch\n')    
    MyDummy.write('../anaconda2/bin/python ABCshell_'+str(i)+'.py')
    MyDummy.close()

 
def BashTheScript(i):
    jobName='submit.'+str(i)+'.pbs'
    #print jobName
    #subprocess.call(['find'])
    subprocess.call(['qsub', '-q', 'cegs', jobName])
    

i=600
while(i<605):
    i=i+1
    #print 'in iter ' + str(i)
    paramsName='ABCshell'
    lineNum=23
    SubmitTheJob(i, paramsName, lineNum)

    
        
    
