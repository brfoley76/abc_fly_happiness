import numpy as np
import math
import scipy as sp
import copy
import sys
import commands
import os
import subprocess
import re
import fileinput
import random
from sklearn import linear_model as lm # for regression
import scipy.stats as st # for getting the exponential distribution
import time
from summaryStats import *
import pandas as pd

class ABCmachine:
    #This is the major iterator. Reads in a file with all the details,
    # iteratively runs Replicate simulations, and gets a fit statistic, simulation.fitStat, from them.
    def __init__(self):
        import simParameters02 as par
        import simulation_functions as sim
        ranseed=999
        self.diagnostics=0
        #do some diagnostic stuff
        if self.diagnostics==1:
            self.diagnosticFile='diagnoses'+str(ranseed)+'.txt'
            f=open(self.diagnosticFile, 'w')
            f.write('Initialised.\n')
            f.close()
        self.Parms=par.Parms
        self.ParmPriors=par.ParmPriors
        self.mutaVec=[]
        for key in self.ParmPriors:
            self.mutaVec.append(key)# create ordered list, to efficiently mutate parameters
        self.moveStats=par.moveStats #dict of target stats1 for simulation
        self.sampleStats=par.sampleStats #dict of target stats 2 for simulation
        self.ABCParms=par.ABCParms #dict of parameters for ABC
        #ordered list of the summary stat results for diagnostic output, 
        #and associated dict for recording.
        self.StatVecOut=par.StatVecOut
        self.outStatRecord={}
        for key in self.StatVecOut:
            self.outStatRecord[key]=[] #will keep all the running stats outputs
        #ordered list of the mutable parameters for output
        self.orderedOutColumns=par.orderedOutColumns
        self.paramStateRecord={}
        for key in self.orderedOutColumns:
            self.paramStateRecord[key]=[] #will hold the parameter and fit statistic info
        
        self.acceptanceRateThreshold=self.ABCParms['acceptanceRate']  #if I fall below this, start flexing again.  
        self.myRanseed=ranseed
        random.seed(ranseed) 
        self.statFileName=self.ABCParms['output']+'_stats_'+str(ranseed)+'.csv'
        self.fitFileName=self.ABCParms['output']+'_fit_'+str(ranseed)+'.csv'
        
        #do some diagnostic stuff
        if self.diagnostics==1:
            f=open(self.diagnosticFile, 'a+')
            f.write('Gonna do my first search.\n')
            f.close()
        while par.initThreshold>=par.searchThreshold: #try a bunch of random combinations until something is reasonable.
            if self.ABCParms['mutValues']==0:
                self.setRandomStart()
            if par.flexiThreshold==1:
                simRep=sim.Replicate(self.Parms, self.moveStats,self.sampleStats, Iteration=0, myRanseed=self.myRanseed)
                par.initThreshold=simRep.fitStat
            self.ABCParms['Threshold']=simRep.fitStat
            print 'Tried. Threshold is '+str(par.initThreshold)
        #do some diagnostic stuff
        if self.diagnostics==1:
            f=open(self.diagnosticFile, 'a+')
            f.write('My initial threshold is '+str(initThreshold)+'.\n')
            f.close()
                
            
            
    def setRandomStart(self):  
        for par in self.ParmPriors:
            parFloor1=self.ParmPriors[par][0]
            parCeil1=self.ParmPriors[par][1]
            parMed=(parFloor1+parCeil1)/2
            parFloor=parMed-(parMed-parFloor1)/4
            parCeil=parMed+(parCeil1-parMed)/4
            startPar=random.uniform(parFloor, parCeil)  
            self.Parms[par]=startPar

            
    def runABC(self):
        #runs through the burn in period, without recording, then the actual MCMC with records
        #prints to file at intervals.
        import simulation_functions as sim
        nPars=len(self.mutaVec)-1
        self.Iteration=0
        self.Accepts=0        
        timeVec=[] 
        self.writeStats(timeVec)
        while self.Iteration < self.ABCParms['burnIn']:
            #print str(self.Iteration)
            self.runARep(nPars, sim)
            self.Iteration=self.Iteration+1
        lastTime=time.clock()   
        while self.Iteration < self.ABCParms['runLength']:
            #print str(self.Iteration)
            simRep=self.runARep(nPars, sim)
            self.Iteration=self.Iteration+1
            if self.Iteration % self.ABCParms['storeInterval']==0:
                self.recordStats(simRep)
                newTime=time.clock()
                timeVec.append(newTime-lastTime)
                lastTime=newTime
            if self.Iteration % self.ABCParms['reportInterval']==0:
                self.writeStats(timeVec)
                timeVec=[]
 

    def writeStats(self,timeVec):   
        #dicts are unordered, so I'll make an ordered vector for output: df = pd.DataFrame(data, columns=data.keys())
        #df.to_csv('my_csv.csv', mode='a', header=False).
        dfFitStats=pd.DataFrame.from_dict(self.paramStateRecord)
        dfFitStats=dfFitStats[self.orderedOutColumns]
        dfFitStats=dfFitStats.assign(Time=timeVec)
        #empty the dict  
        for key in self.paramStateRecord:
            self.paramStateRecord[key]=[]
        if self.Iteration>0:
            #print 'should be writing'
            dfFitStats.to_csv(self.fitFileName, mode='a', header=False)            
            if self.ABCParms['writeDiagnosticStats']==1:
                dfStatRec=pd.DataFrame.from_dict(self.outStatRecord)
                dfStatRec=dfStatRec[self.StatVecOut]
                dfStatRec.insert(0, 'Iteration',dfFitStats['Iteration'])
                dfStatRec.to_csv(self.statFileName, mode='a', header=False, sep='&')
                for key in self.outStatRecord:
                    self.outStatRecord[key]=[]
        else:
            dfFitStats.to_csv(self.fitFileName, mode='w', header=True)
            if self.ABCParms['writeDiagnosticStats']==1:
                dfStatRec=pd.DataFrame.from_dict(self.outStatRecord)
                dfStatRec=dfStatRec[self.StatVecOut]
                dfStatRec.insert(0, 'Iteration', dfFitStats['Iteration'])
                dfStatRec.to_csv(self.statFileName, mode='w', header=True, sep='&')
  

    def recordStats(self, simRep):
        #update a record
        for key in simRep.statOutDict:
            self.outStatRecord[key].append(simRep.statOutDict[key])
        for key in self.ParmPriors:
            self.paramStateRecord[key].append(self.Parms[key])       
        self.paramStateRecord['Iteration'].append(self.Iteration)
        self.paramStateRecord['Accepts'].append(self.Accepts)
        self.paramStateRecord['Threshold'].append(self.ABCParms['Threshold'])
        self.paramStateRecord['AcceptanceRate'].append(self.ABCParms['acceptanceRate'])

        
    def runARep(self, nPars, sim):
        # Mutates, runs a single round of simulation, and either accepts or rejects a move
        #returns a Replicate object
        ratchet=self.ABCParms['ratchetUp']
        counterRatchet=1-ratchet
        oldState=self.proposeNewState(nPars)
        #use Paul's acceptance thingy
        nSims=self.ABCParms['numberOfSimsPerAttempt']
        neededAccepts=math.ceil(nSims*random.random())
        possibleFails=nSims-neededAccepts
        i=0
        while not(possibleFails<0) and neededAccepts>0:
            simRep=sim.Replicate(self.Parms,self.moveStats,self.sampleStats,self.Iteration,self.myRanseed)
            fit=simRep.fitStat
            if fit>self.ABCParms['Threshold']:
                possibleFails=possibleFails-1
            else:
                neededAccepts=neededAccepts-1    
        if possibleFails<0: #fail
            self.ABCParms['acceptanceRate']=self.ABCParms['acceptanceRate']*0.95 #
            #print 'Failed. Acceptance rate is :'+ str(self.ABCParms['acceptanceRate'])
            self.Parms[oldState[0]]=oldState[1] #return
            if self.ABCParms['flexiThreshold']==1: #can the threshold go up and down?
                self.ABCParms['Threshold']=self.ABCParms['Threshold']*counterRatchet+fit*ratchet
            if self.ABCParms['acceptanceRate']<self.acceptanceRateThreshold:#resume flexi
                self.ABCParms['flexiThreshold']=1
        else: #accept!
            self.ABCParms['acceptanceRate']=1.0-(1.0-self.ABCParms['acceptanceRate'])*0.9
            #print 'Accepted! Acceptance rate is :'+ str(self.ABCParms['acceptanceRate'])
            if self.ABCParms['flexiThreshold']==1:  #need to adjust the threshold. 
                if fit<self.ABCParms['Threshold']:
                    self.ABCParms['Threshold']=fit*0.5+self.ABCParms['Threshold']*0.5
                self.Accepts=self.Accepts+1
                if fit<=self.ABCParms['finalThreshold']:# sets the fixed threshold
                    self.ABCParms['Threshold']=self.ABCParms['finalThreshold']
                    self.ABCParms['flexiThreshold']=0
            else:
                self.Accepts=self.Accepts+1
                if self.ABCParms['acceptanceRate']<self.acceptanceRateThreshold:
                    self.ABCParms['flexiThreshold']=0
        return simRep


    def proposeNewState(self, nPars):
        #returns a list, oldState, with index=0 is the key, and index=1 is the old value
        selector=random.randint(0,nPars)
        mutakey=self.mutaVec[selector]
        lowerBound=self.ParmPriors[mutakey][0]
        upperBound=self.ParmPriors[mutakey][1]
        parRange=self.ParmPriors[mutakey][2]
        oldState=[mutakey, self.Parms[mutakey]]
        if random.random<self.ABCParms['bigLittleRatio']: #big jump
            q=random.random()-0.5
            magnitudeChange=self.ABCParms['bigJump']*parRange*q
        else: #big jump
            q=random.random()-0.5
            magnitudeChange=self.ABCParms['littleJump']*parRange*q
        newVal=self.Parms[mutakey]+magnitudeChange
        #reflect if the value gets too big or little
        if newVal>upperBound:
            newVal=upperBound-(newVal-upperBound)
        if newVal<lowerBound:
            newVal=lowerBound-(newVal-lowerBound) 
        self.Parms[mutakey]=newVal #I think this is the only case I change a self.Parms value
        return oldState


aaa=ABCmachine()
aaa.runABC()
