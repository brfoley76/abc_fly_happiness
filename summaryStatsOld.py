import numpy as np
from sklearn import linear_model as lm # for regression
import scipy.stats as st # for getting the exponential distribution

def regMPerM(SampleRecord, standards):
    # regression coeff. of males-per-female with time
    #print 'regMPerM'
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            mPerM=SampleRecord.p0M*(SampleRecord.p0M-1)+SampleRecord.p1M*(SampleRecord.p1M-1)
            mmMod=st.linregress(SampleRecord.IntEnd,mPerM)
            mmSlope=mmMod.slope
            mmIntercept=mmMod.intercept
            mmSlope=(mmSlope-meanSlope)/sdSlope
            mmIntercept=(mmIntercept-meanIntercept)/sdIntercept
        except:
            mmSlope,mmIntercept=11,11
        if np.isnan(mmSlope):
            mfSlope=11
            mmIntercept=11
    else:
        mfSlope=12
        mmIntercept=12
    return [mmSlope, mmIntercept]


def regMPerF(SampleRecord, standards):
    # regression coeff. of males-per-female with time
    #print 'regMPerF'
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            mPerF=SampleRecord.p0F*SampleRecord.p0M+SampleRecord.p1F*SampleRecord.p1M
            mfMod=st.linregress(SampleRecord.IntEnd,mPerF)
            mfSlope=mfMod.slope
            mfIntercept=mfMod.intercept
            mfSlope=(mfSlope-meanSlope)/sdSlope
            mfIntercept=(mfIntercept-meanIntercept)/sdIntercept
        except:
            mfSlope,mfIntercept=11,11
        if np.isnan(mfSlope):
            mfSlope=11
            mfIntercept=11
    else:
        mfSlope=12
        mfIntercept=12
    return [mfSlope, mfIntercept]


def regPropFoodF(SampleRecord, standards):
    # regression of log likelihood female-being-on-food, with logtime
    #print 'regPropFoodF'
    if len(SampleRecord)>1:
        try:
            meanSlope=standards[0]
            sdSlope=standards[1]
            meanIntercept=standards[2]
            sdIntercept=standards[3]
            X=SampleRecord.p1F.values
            y=SampleRecord.IntEnd
            foodModF=st.linregress(X,y)
            foodSlopeF=foodModF.slope
            foodInterceptF=foodModF.intercept
            foodSlopeF=(foodSlopeF-meanSlope)/sdSlope
            foodInterceptF=(foodInterceptF-meanIntercept)/sdIntercept
        except:    
            foodSlopeF,foodInterceptF=11,11
        if np.isnan(foodSlopeF):
            foodSlopeF=11
            foodInterceptF=11
    else:
        foodSlopeF=12
        foodInterceptF=12
    return [foodSlopeF, foodInterceptF]


def regPropFoodM(SampleRecord, standards):
    # regression of log likelihood female-being-on-food, with logtime
    #print 'regPropFoodM'
    if len(SampleRecord)>1:
        meanSlope=standards[0]
        sdSlope=standards[1]
        meanIntercept=standards[2]
        sdIntercept=standards[3]
        try:
            X=SampleRecord.p1M.values
            y=SampleRecord.IntEnd
            foodModM=st.linregress(X,y)
            foodSlopeM=foodModM.slope
            foodInterceptM=foodModM.intercept
            foodSlopeM=(foodSlopeM-meanSlope)/sdSlope
            foodInterceptM=(foodInterceptM-meanIntercept)/sdIntercept
        except:
            foodSlopeM,foodInterceptM=11,11
        if np.isnan(foodSlopeM):
            foodSlopeM=11
            foodInterceptM=11
    else:
        foodSlopeM=12
        foodInterceptM=12
    return [foodSlopeM, foodInterceptM]


def meanDurationSub1(dfMoves, standards):
    #besides the last interval (tacked on), how long are the intervals? This should be the most important
    #stat, btw, in the initial exploration phase.
    #print 'meanDurationSub1'
    mean=standards[0]
    sd=standards[1]
    singleFly=dfMoves.iloc[0,:].FlyId
    try:
        durVec=dfMoves[dfMoves['FlyId']==singleFly].Duration
        durVec=durVec.iloc[range(0, (len(durVec)-1))]
        durMean=np.mean(durVec)
        durMean=(durMean-mean)/sd
    except:
        durMean=11
    return [durMean]


def LikFoodF(dfMoves, standards):
    # mean proportion of time females spend on food
    #print 'LikFoodF'
    mean=standards[0]
    sd=standards[1]
    try:
        On=np.sum(dfMoves[(dfMoves['Sex']==0) & (dfMoves['Food']==1)].Duration)+1
        Off=np.sum(dfMoves[(dfMoves['Sex']==0) & (dfMoves['Food']==0)].Duration)+1
        fLikeFood=np.log(On/Off)
        fLikeFood=(fLikeFood-mean)/sd
    except:
        fLikeFood=11
    if (np.isinf(fLikeFood) or np.isneginf(fLikeFood)):
        fLikeFood=11
    return [fLikeFood]

def LikFoodM(dfMoves, standards):
    # mean proportion of (log?) time females spend on food
    #print 'LikFoodM'
    mean=standards[0]
    sd=standards[1]
    try:
        On=np.sum(dfMoves[(dfMoves['Sex']==1) & (dfMoves['Food']==1)].Duration)+1
        Off=np.sum(dfMoves[(dfMoves['Sex']==1) & (dfMoves['Food']==0)].Duration)+1
        mLikeFood=np.log(On/Off)
        mLikeFood=(mLikeFood-mean)/sd
    except:
        mLikeFood=11
    if (np.isinf(mLikeFood) or np.isneginf(mLikeFood)):
        mLikeFood=11
    return [mLikeFood]
    

def mPerF(dfMoves, standards):
    # mean number of males-per-female
    #print 'mPerF'
    mean=standards[0]
    sd=standards[1]
    try:
        nM=np.sum((dfMoves[dfMoves['Sex']==0].nMales)*(dfMoves[dfMoves['Sex']==0].Duration))/np.sum(dfMoves[dfMoves['Sex']==0].Duration)
        nM=(nM-mean)/sd
    except:
        nM=11
    if (np.isinf(nM) or np.isneginf(nM)):
        nM=11        
    return [nM]


def mPerM(dfMoves, standards):
    # mean number of males-per-female
    #print 'mPerM'
    mean=standards[0]
    sd=standards[1]
    try:
        nM=np.sum((dfMoves[dfMoves['Sex']==1].nMales -1)*(dfMoves[dfMoves['Sex']==1].Duration))/np.sum(dfMoves[dfMoves['Sex']==1].Duration)
        nM=(nM-mean)/sd
    except:
        nM=11
    if (np.isinf(nM) or np.isneginf(nM)):
        nM=11        
    return [nM]


def nMovesF(dfMoves, standards):
    #count of female moves
    #print 'nMovesF'
    mean=standards[0]
    sd=standards[1]
    try:
        moves=np.sum(dfMoves[dfMoves['Sex']==0].Leave)
        moves=(moves-mean)/sd
    except:
        moves=11
    if (np.isinf(moves) or np.isneginf(moves)):
        moves=11                
    return [moves]


def nMovesM(dfMoves,standards):
    #count of male moves
    #print 'nMovesM'
    mean=standards[0]
    sd=standards[1]
    try:
        moves=np.sum(dfMoves[dfMoves['Sex']==1].Leave)
        moves=(moves-mean)/sd
    except:
        moves=11
    if (np.isinf(moves) or np.isneginf(moves)):
        moves=11                
    return [moves]


def regMovesF(dfMoves, standards):
    #regression of whether-moves-were-made-by-females or not, with time
    #print 'regMovesF'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]    
    try:
        y=dfMoves[dfMoves['Sex']==0].Leave
        X=dfMoves[dfMoves['Sex']==0].logTime
        fMovesMod=st.linregress(X,y)
        fMovesSlope=fMovesMod.slope
        fMovesIntercept=fMovesMod.intercept
        fMovesSlope=(fMovesSlope-meanSlope)/sdSlope
        fMovesIntercept=(fMovesIntercept-meanIntercept)/sdIntercept
    except:
        fMovesSlope,fMovesIntercept=11,11
    if np.isnan(fMovesSlope):
        fMovesSlope=11
        fMovesIntercept=11       
    return [fMovesSlope, fMovesIntercept]


def regDuration(dfMoves, standards):
    #change in duration lengths with time
    #print 'regMovesF'
    meanSlope=standards[0]
    sdSlope=standards[1]
    try:
        singleFly=dfMoves.iloc[0,:].FlyId
        meanIntercept=standards[2]
        sdIntercept=standards[3]    
        y=dfMoves[dfMoves['FlyId']==singleFly].Duration
        X=dfMoves[dfMoves['FlyId']==singleFly].logTime
        durationMod=st.linregress(X,y)
        durSlope=durationMod.slope
        durIntercept=durationMod.intercept
        durSlope=(durSlope-meanSlope)/sdSlope
        durIntercept=(durIntercept-meanIntercept)/sdIntercept
    except:
        durSlope,durIntercept=11,11
    if np.isnan(durSlope):
        durSlope=11
        durIntercept=11               
    return [durSlope, durIntercept]


def regMLikLeaveM(dfMoves, standards):
    #change in male leaving likelihood with n males present
    #print 'regMLikLeaveM'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==1),'nMales'].values.reshape(-1,1)
        sample_weight=dfMoves.loc[(dfMoves['Sex']==1),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==1),('Leave')]
        mLeaveMmod=log_model.fit(X,y, sample_weight=sample_weight)
        mLeaveMslope=mLeaveMmod.coef_[0][0]
        mLeaveMslope=(mLeaveMslope-meanSlope)/sdSlope
        mLeaveMint=mLeaveMmod.intercept_[0]
        mLeaveMint=(mLeaveMint-meanIntercept)/sdIntercept
    except:
        mLeaveMslope,mLeaveMint=11,11
    if np.isnan(mLeaveMslope):
        mLeaveMslope=11
        mLeaveMint=11                 
    return [mLeaveMslope, mLeaveMint]


def regMLikLeaveF(dfMoves,standards):
    #change in male leaving likelihood with n females present
    #print 'regMLikLeaveF'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==1),'nFemales'].values.reshape(-1,1)
        sample_weight=dfMoves.loc[(dfMoves['Sex']==1),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==1),('Leave')]
        mLeaveFmod=log_model.fit(X,y, sample_weight=sample_weight)
        mLeaveFslope=mLeaveFmod.coef_[0][0]
        mLeaveFslope=(mLeaveFslope-meanSlope)/sdSlope
        mLeaveFint=mLeaveFmod.intercept_[0]
        mLeaveFint=(mLeaveFint-meanIntercept)/sdIntercept    
    except:
        mLeaveFslope,mLeaveFint=11,11
    if np.isnan(mLeaveFslope):
        mLeaveFslope=11
        mLeaveFint=11                       
    return [mLeaveFslope, mLeaveFint]


def regFLikLeaveM(dfMoves, standards):   
    #change in female leaving likelihood with n males present
    #print 'regFLikLeaveM'
    meanSlope=standards[0]
    sdSlope=standards[1]
    meanIntercept=standards[2]
    sdIntercept=standards[3]
    try:
        log_model=lm.LogisticRegression()
        X=dfMoves.loc[(dfMoves['Sex']==0),'nMales'].values.reshape(-1,1)
        sample_weight=dfMoves.loc[(dfMoves['Sex']==0),('Duration')]
        y=dfMoves.loc[(dfMoves['Sex']==0),('Leave')]
        fLeaveMmod=log_model.fit(X,y, sample_weight=sample_weight)
        fLeaveMslope=fLeaveMmod.coef_[0][0]
        fLeaveMslope=(fLeaveMslope-meanSlope)/sdSlope
        fLeaveMint=fLeaveMmod.intercept_[0]
        fLeaveMint=(fLeaveMint-meanIntercept)/sdIntercept    
    except:
        fLeaveMslope,fLeaveMint=11,11
    if np.isnan(fLeaveMslope):
        mLeaveFslope=11
        fLeaveMint=11                               
    return [fLeaveMslope, fLeaveMint]


