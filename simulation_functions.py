import numpy as np
import pandas as pd
import math
import scipy as sp
import copy
import sys
import commands
import random
import scipy.stats as st # for getting the exponential distribution
from summaryStats import *

#columns=['Time','FlyId','Sex','Patch','Food','nFemales','nMales','Leave' ]
class Fly:
    #The individual, with their genetic proclivities, and experience 
    def __init__(self,Sex,Pars,Key):
        self.Sex=Sex
        if self.Sex==0:
            self.FoodPref=Pars['fFoodPref']
            self.FemalePref=Pars['fFemalePref']
            self.MalePref=Pars['fMalePref']
            self.Inertia=Pars['fInertia']
            self.Learn=Pars['fLearn']
        if Sex==1:
            self.FoodPref=Pars['mFoodPref']
            self.FemalePref=Pars['mFemalePref']
            self.MalePref=Pars['mMalePref']
            self.Inertia=Pars['mInertia']
            self.Learn=Pars['mLearn']
        self.Happiness=None 
        self.PatchID=None

class Patch:
    #The general areas a fly can be in. Their state is defined by the food, and the flies that are there
    #Since the flies are identical (when of the same sex), I'll keep track of 
    def __init__(self,Food,ID):
        self.Food=Food
        self.nFemales=0
        self.nMales=0
        #self.FliesHere={} this could be handy, but not necessary now
        self.PatchID=ID
        #to streamline the calculations when all individuals of each sex are identical.
        self.GenericMaleHappiness=None
        self.GenericFemaleHappiness=None

class Replicate:
    #This replicate is the container, that takes in the vector of parameters passed to it by the ABC algorithm,
    #and has summary statistics, self.sampOut, self.moveOut
    def __init__(self, Parms,moveStats,sampleStats,Iteration,myRanseed):
        self.OutputLabel=myRanseed
        self.Iteration=Iteration
        self.Pars=Parms
        #initialise the dicts containing the sim outcomes
        self.sampOut={}
        self.moveOut={}

        appendix=["_1", "_2"] #for the most part '_1' is the slope, and '_2' is the intercept. But not always.
        for stat in moveStats:
            nMeasures=len(moveStats[stat][1])/2
            self.moveOut[stat]=[]
            for i in range(0,nMeasures):
                app=stat+appendix[i]
                self.moveOut[stat].append([app])       
        for stat in sampleStats:
            nMeasures=len(sampleStats[stat][1])/2
            self.sampOut[stat]=[]
            for i in range(0,nMeasures):
                app=stat+appendix[i]
                self.sampOut[stat].append([app]) 
        for i in range(0,self.Pars['nReps']):
            self.RunSim(moveStats,sampleStats)   
        self.fitStat, self.statOutDict=self.CalcFitStat()


    def CalcFitStat(self):
        #The overall fit statistic will be the Euclidian distance of all statistics of all the runs from 0
        #won't include the variance around the fit, since not modelling between replicate error
        summedStat=0
        statOutDict={}
        for stat in self.sampOut:
            for i in range(0, len(self.sampOut[stat])):
                myMean=np.mean(self.sampOut[stat][i][1:len(self.sampOut[stat][i])])
                statOutDict[self.sampOut[stat][i][0]]=myMean
                myMean=np.power(myMean, 2)
                myStd=np.std(self.sampOut[stat][i][1:len(self.sampOut[stat][i])])
                myStd=np.power(np.log(myStd+0.00001), 2) #0.00001 puts effect of null std deviation on same scale as null stat
                summedStat=summedStat+myMean
                
        for stat in self.moveOut:
            for i in range(0, len(self.moveOut[stat])):
                myMean=np.mean(self.moveOut[stat][i][1:len(self.moveOut[stat][i])]) 
                statOutDict[self.moveOut[stat][i][0]]=myMean
                myMean=np.power(myMean, 2)
                summedStat=summedStat+myMean
        summedStat=np.power(summedStat,0.5)        
        return summedStat, statOutDict
               
                       
    def MakeFlies(self):
        # create the objects that are doing the moving
        index=self.Pars['nFemales']
        Flies={}
        for ind in range(0,index):
            newFly=Fly(0,self.Pars,ind)
            Flies[ind]=newFly
        totFlies=index+self.Pars['nMales']    
        for ind in range(index,totFlies):
            newFly=Fly(1,self.Pars,ind)
            Flies[ind]=newFly
        self.Flies=Flies  

        
    def MakePatches(self):
        #make patch 0 the non-food patch, for simplicity
        index=self.Pars['nPatches']-self.Pars['nFood']
        Patches={}
        for patch in range(0,index):
            newPatch=Patch(0,patch)
            Patches[patch]=newPatch 
        for patch in range(index,self.Pars['nPatches']):
            newPatch=Patch(self.Pars['FoodVal'],patch)
            Patches[patch]=newPatch
        self.Patches=Patches    


    def InitialiseFlies(self):
        #put them all on the non-food patch to start. Can make this more flexible later, if necessary
        for fly in self.Flies:
            self.Flies[fly].PatchID=0
            if self.Flies[fly].Sex==0:
                self.Patches[0].nFemales=self.Patches[0].nFemales+1
            else:
                self.Patches[0].nMales=self.Patches[0].nMales+1       
        self.UpdatePatches()
            

    def calcFemaleHappiness(self,ThisPatch):
        # standard cox proportional hazard model
        #Happiness is a linear function of prefs and states.
        #as in a CoxPH model, lambda=lambda_0*exp(beta_i x_i)
        #for simplicity (and to keep things symmetrical around 0, I'll stick lambda_0 in the exponent)
        logTime=np.log(self.TimeFromStart+1)
        happiness=(self.Pars['fFoodPref']*ThisPatch.Food+self.Pars['fFemalePref']*(ThisPatch.nFemales-1)+
                   self.Pars['fMalePref']*ThisPatch.nMales)*self.Pars['fLearn']*logTime+self.Pars['fInertia']
        if happiness>700: #avoid ridiculously large values that break the math
            happiness=700
        happiness=np.exp(happiness)
        if happiness<=0:
           happiness=np.exp(-700.0)
        return happiness
        
        
    def calcMaleHappiness(self,ThisPatch):
        #male homologue of female happiness
        logTime=np.log(self.TimeFromStart+1)
        happiness=(self.Pars['mFoodPref']*ThisPatch.Food+self.Pars['mFemalePref']*ThisPatch.nFemales+
                   self.Pars['mMalePref']*(ThisPatch.nMales-1))*self.Pars['mLearn']*logTime+self.Pars['mInertia']
        if happiness>700:
            happiness=700.0                  
        happiness=np.exp(happiness)
        if happiness<=0:
            happiness=np.exp(-700.0)
        return happiness      

    def CalcNextMove(self):   
        #run through the flies. The one with the lowest moving time is the next to move
        minTime=self.TimeLeft
        minFly=-1
        StopIt=0
        for fly in self.Flies:
            flyTime=self.CalcExpSurvival(self.Flies[fly].Happiness)
            if flyTime<minTime:
                minTime=flyTime
                minFly=fly
        #if minFly==-1:
        #    self.TimeLeft=0
        return minFly, minTime


    def CalcExpSurvival(self, Happiness):
        # returns a random survival time drawn from the correct distribution
        #dist=st.expon(0., Happiness)
        #rF=dist.ppf
        survival=np.random.exponential(Happiness)+10 # need a floor
        return survival

    
    def Move(self, MoveFly):
        #for now we'll stick with our old paradigm where a single "off patch" state is described by patch==0
        #and flies move from being on patches, to patch 0
        #or from patch==0 to a random patch (1,nPatches)
        CurrentPatch=self.Flies[MoveFly].PatchID
        MoveFlySex=self.Flies[MoveFly].Sex
        if CurrentPatch==0: #the fly is off patch
            if self.Pars['nFood']>2:
                NextPatch=random.randint(1,self.Pars['nFood'])
            else:
                NextPatch=1
        else:
            NextPatch=0
        self.Flies[MoveFly].PatchID=NextPatch
        if MoveFlySex==0:
            self.Patches[CurrentPatch].nFemales=self.Patches[CurrentPatch].nFemales-1
            self.Patches[NextPatch].nFemales=self.Patches[NextPatch].nFemales+1
        else:    
            self.Patches[CurrentPatch].nMales=self.Patches[CurrentPatch].nMales-1
            self.Patches[NextPatch].nMales=self.Patches[NextPatch].nMales+1
        Check=0
        if Check==1:
            self.CheckPatchState()  


    def CheckPatchState(self):
        #run through the patches and make sure none of them have an impossible number of flies
        for patch in self.Patches:
            if self.Patches[patch].nFemales<0:
                print "patch number " + str(patch) + "has " + str(self.Patches[patch].nFemales) + "females" 
            if self.Patches[patch].nFemales>self.Pars['nFemales']:
                print "patch number " + str(patch) + "has " + str(self.Patches[patch].nFemales) + "females" 
            if self.Patches[patch].nMales<0:
                print "patch number " + str(patch) + "has " + str(self.Patches[patch].nMales) + "males" 
            if self.Patches[patch].nMales>self.Pars['nMales']:
                print "patch number " + str(patch) + "has " + str(self.Patches[patch].nMales) + "males" 
                

    def UpdatePatches(self):
        #Go through the patches, update the expected happiness
        # go through the flies, update their happiness
        for patch in self.Patches:
            if self.Patches[patch].nFemales>0:
                self.Patches[patch].GenericFemaleHappiness=self.calcFemaleHappiness(self.Patches[patch])
            else: 
                self.Patches[patch].GenericFemaleHappiness=None
            if self.Patches[patch].nMales>0:
                 self.Patches[patch].GenericMaleHappiness=self.calcMaleHappiness(self.Patches[patch])
            else: 
                self.Patches[patch].GenericMaleHappiness=None

        for fly in self.Flies:
            if self.Flies[fly].Sex==0:
                self.Flies[fly].Happiness=self.Patches[self.Flies[fly].PatchID].GenericFemaleHappiness
            else:
                self.Flies[fly].Happiness=self.Patches[self.Flies[fly].PatchID].GenericMaleHappiness        

        
    def UpdateSamples(self, nextMeasure, measureIndex):
        #this is ad-hoc, fragile, and contingent on the precise current setup.
        #for patch in self.Patches:
        #print measureIndex
        self.SampleRecord[measureIndex][4]=nextMeasure
        for patch in self.Patches:
            slot=patch*2
            self.SampleRecord[measureIndex][slot]=self.Patches[patch].nFemales
            self.SampleRecord[measureIndex][(slot+1)]=self.Patches[patch].nMales
            
    
    def UpdateRecords(self, MoveFly, TimeElapsed):
        # StateRecord contains a full account of fly movement during the experiment 
        for fly in self.Flies:
            patch=self.Flies[fly].PatchID
            newLine=[self.TimeFromStart, self.Moves, fly, self.Flies[fly].Sex, patch]
            newLine.extend((self.Patches[patch].Food, self.Patches[patch].nFemales, self.Patches[patch].nMales,TimeElapsed))                
            if fly==MoveFly:
                newLine.extend([1,self.Flies[fly].Happiness])
            else:
                newLine.extend([0,self.Flies[fly].Happiness])
            self.StateRecord.append(newLine)
            
     
    def writeThingsToFile(self):
        #dummy function for printing out dataframes for investigation
        movesName='dfMoves_'+str(self.OutputLabel)+'_itNo_'+str(self.Iteration)+'.csv'
        sampsName='dfSamps_'+str(self.OutputLabel)+'_itNo_'+str(self.Iteration)+'.csv'
        self.dfMoves.to_csv(movesName, mode='w', header=True)
        dfSamps=pd.DataFrame(self.SampleRecord)


    def CalcStats(self,MoveFly,TimeElapsed,moveStats,sampleStats, measureIndex):
        #truncate SampleRecord to only contain samples taken
        sampleRecord=self.SampleRecord[:(measureIndex-1)]
        nameVec=['p0F','p0M','p1F','p1M','IntEnd']
        sampleRecord=pd.DataFrame(sampleRecord, columns=nameVec)
        # add the final row to StateRecord
        for fly in self.Flies:
            patch=self.Flies[fly].PatchID
            newLine=[self.Pars['Seconds'], self.Moves, fly, self.Flies[fly].Sex, patch]
            newLine.extend((self.Patches[patch].Food, self.Patches[patch].nFemales, self.Patches[patch].nMales,TimeElapsed))
            if fly==MoveFly:
                newLine.extend([1,self.Flies[fly].Happiness])
            else:
                newLine.extend([0,self.Flies[fly].Happiness])
            self.StateRecord.append(newLine)
        #make it into a pandas dataframe. 
        self.dfMoves=pd.DataFrame(self.StateRecord, columns=['Time','Moves', 'FlyId','Sex','Patch','Food','nFemales','nMales','Duration', 'Leave', 'Happiness'])
        self.dfMoves['logTime']=pd.Series(self.dfMoves.loc[:,'Time'], index=self.dfMoves.index)
        self.dfMoves['logTime']=np.log(self.dfMoves["logTime"]+1)
        #log duration is more complicated, because it's log time_(t+1) - log time_t
        self.dfMoves['logDuration']=pd.Series(self.dfMoves.loc[:,'Time'], index=self.dfMoves.index)
        self.dfMoves['logDuration']=self.dfMoves.loc[:,'logTime']-np.log(self.dfMoves.loc[:,'Time']-self.dfMoves["Duration"]+1)
        #if self.Iteration>160:
        #    print 'I\'m in my %d iteration' %  (self.Iteration)
        #if self.Iteration % 500 == 0:
        #    self.writeThingsToFile()         
        for fun in sampleStats:
            Do=sampleStats[fun]
            output=Do[0](sampleRecord, Do[1])
            for i in range(0, len(output)):
                self.sampOut[fun][i].append(output[i])
        for fun in moveStats:
            Do=moveStats[fun]
            output=Do[0](self.dfMoves,Do[1])
            for i in range(0, len(output)):
                self.moveOut[fun][i].append(output[i])               
 
    
    def RunSim(self, moveStats,sampleStats):
        # a single simulation run
        self.TimeFromStart=0
        self.StateRecord=[] # a record of every move, and state at each move
        self.MakeFlies()
        self.MakePatches()
        self.InitialiseFlies()
        #for the SampleRecord array (np.array ~99x faster than pd.dataframe)
        totMeasures=self.Pars['nIntervals']+1
        Length=np.log((float(self.Pars['Seconds'])-10))/float(totMeasures)
        nameVec=["p0F","p0M","p1F","p1M"]
        self.SampleRecord=np.zeros((totMeasures,5))   # evenly spaced sampling intervals, and a cumulative description at each interval 
        nextMeasure=Length
        # update two kinds of records. Those done whenever there is a move ; 
        #and those done at even time intervals.
        self.TimeLeft=self.Pars['Seconds'] 
        measureIndex=0 #the index of the array element we're updating
        self.Moves=0
        while self.TimeLeft>0:      
            MoveFly, TimeElapsed=self.CalcNextMove()
            if MoveFly > -1: #there is some fly who will move, before the end of the experiment
                self.Moves=self.Moves+1
                self.TimeFromStart=self.TimeFromStart+TimeElapsed
                while (np.log(self.TimeFromStart+1)>=nextMeasure) & (measureIndex<totMeasures):
                    #update the evenly-measured samples
                    self.UpdateSamples(nextMeasure, measureIndex)
                    measureIndex=measureIndex+1
                    nextMeasure=nextMeasure+Length
                self.UpdateRecords(MoveFly, TimeElapsed)
                self.Move(MoveFly)
                self.UpdatePatches()
                self.TimeLeft=self.TimeLeft-TimeElapsed
            else:
                TimeElapsed=self.TimeLeft
                self.TimeLeft=0
            if self.Moves>self.Pars['maxMoves']:
                TimeElapsed=self.TimeLeft
                self.TimeLeft=0     
        self.CalcStats(MoveFly, TimeElapsed, moveStats,sampleStats, measureIndex)

 
