import pandas as pd
import numpy as np
from summaryStats import *

#set parameter values
#ranseed=605


# ABC 
bigJump=0.5 # proportion of the total prior range that a parameter might be mutated
littleJump=0.1 # proportion of the total prior range that a parameter might be mutated
bigLittleRatio=0.2
mutValues=0 #either use set initial parameters (1), or choose from the prior (0)
burnIn=10
storeInterval=25 #store the state at intervals of this many iterations
reportInterval=100 #report the results at intervals of this many iterations
runLength=999999999
output='Flexioutfile_02'
writeDiagnosticStats=1 #do we write out the value of each individual stat?

#### for flexible threshold simulations
flexiThreshold=1 # have an acceptance threshold that ratchets down slowly (1) or a fixed threshold (0)
Threshold=0 # the fixed initial threshold if flexiThreshold==0, or the variable one if flexiThreshold==1
finalThreshold=-1 # a target threshold to begin fixed-threshold simulations and recording.
ratchetUp=0.2 # when in the initial wandering stage, allow the threshold to drift up again
numberOfSimsPerAttempt=5 #simulate up to this many times to assess fit
initThreshold=2500
searchThreshold=13
acceptanceRate=0.15 # if we stop accepting new proposals for too long, we're probably stuck, and need to go back to flexible mode again.

#Dict of ABC parameters
ABCParms={'bigJump': bigJump,'littleJump':littleJump,'bigLittleRatio':bigLittleRatio, 'mutValues':mutValues,'burnIn':burnIn,'output':output,'flexiThreshold':flexiThreshold, 'Threshold':Threshold, 'finalThreshold': finalThreshold,'ratchetUp':ratchetUp,'storeInterval':storeInterval, 'reportInterval':reportInterval, 'runLength':runLength, 'writeDiagnosticStats':writeDiagnosticStats,'numberOfSimsPerAttempt':numberOfSimsPerAttempt,
'acceptanceRate':acceptanceRate}

#simulation, fixed parameters
nReps=8 #output from calcStatsFromExperiment.py
nMales=2
nFemales=0
nPatches=2
nFood=1
FoodVal=1.0
Seconds=216000
nIntervals=100
#output from calcStatsFromExperiment.py
maxMoves=124*3 #the most moves that are allowed in a simulation, 3x the max observed no. 

#simulation, mutable parameters
fFoodPref=0.0 #this is gonna be null. Deleted from mutable parameters.
mFoodPref=3.0
fFemalePref=0.0 #this is gonna be null. Deleted from mutable parameters.
mFemalePref=0.0 #this is gonna be null. Deleted from mutable parameters.
fMalePref=0.0 #this is gonna be null. Deleted from mutable parameters.
mMalePref=-1.0 
fInertia=0.0 #this is gonna be null. Deleted from mutable parameters.
mInertia=10.0
fLearn=0.0 #this is gonna be null. Deleted from mutable parameters.
mLearn=2.0

#Dict of all simulation parameters
Parms={'fFoodPref' : fFoodPref, 'mFoodPref':mFoodPref,'fFemalePref':fFemalePref,'mFemalePref':mFemalePref,
         'fMalePref':fMalePref,'mMalePref':mMalePref,'fInertia':fInertia,'mInertia':mInertia,
         'fLearn':fLearn,'mLearn':mLearn,'nFemales':nFemales,'nMales':nMales,'nPatches':nPatches,'nFood':nFood,
         'FoodVal':FoodVal,'Seconds':Seconds, 'nIntervals':nIntervals,'maxMoves':maxMoves, 'nReps':nReps}
#Dict of mutable parameters, and their priors
ParmPriors={'mFoodPref':[-20,20,40],'mMalePref':[-20,20,40],'mInertia':[0,15,15],'mLearn':[0,20,20]}

#Dicts of statistics, and their target values
moveStats={}
sampleStats={}
sampleStats['regMPerM']=[regMPerM,[-0.232375191317,0.518946796179,2.14568439776,0.642936675238]]
sampleStats['regPropFoodM']=[regPropFoodM,[0.182007318905,0.0466584830468,0.469165614773,0.42282295952]]

moveStats['regMLikLeaveFood']=[regMLikLeaveFood,[-1.46282301529,1.72217263447,0.686459350507,0.854910445928]]
moveStats['LikFoodM']=[LikFoodM,[1.06692250964,1.05111776778]]
moveStats['meanDurationSub1']=[meanDurationSub1,[100.117512403,43.6714499553]]
moveStats['regMLikLeaveM']=[regMLikLeaveM,[0.618696440985,0.904187468394]]
moveStats['nMovesM']=[nMovesM,[84.75,40.1427141584]]
moveStats['regDuration']=[regDuration,[-0.177248236781,0.666235245522,5.13775469373,5.55529810749]]
moveStats['mPerM']=[mPerM,[0.756368634259,0.141142625033]]

#dicts are unordered, so I'll make an ordered vector for output: df = pd.DataFrame(data, columns=data.keys())
#df.to_csv('my_csv.csv', mode='a', header=False)
#These have extra parameters, not in the dicts themselves.
orderedOutColumns=['Iteration','Accepts','Threshold','AcceptanceRate', 'mFoodPref','mMalePref','mInertia','mLearn']

StatVecOut=['regPropFoodM_1','regPropFoodM_2','regMPerM_1','regMPerM_2',
'meanDurationSub1_1','regDuration_1','regDuration_2','LikFoodM_1','mPerM_1','nMovesM_1',
'regMLikLeaveFood_1','regMLikLeaveFood_2','regMLikLeaveM_1']


