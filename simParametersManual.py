import pandas as pd
import numpy as np
from summaryStats import *

#set parameter values
ranseed=4

# ABC 
bigJump=0.5 # proportion of the total prior range that a parameter might be mutated
littleJump=0.1 # proportion of the total prior range that a parameter might be mutated
bigLittleRatio=0.2
mutValues=0 #either use set initial parameters (1), or choose from the prior (0)
burnIn=1
storeInterval=5 #store the state at intervals of this many iterations
reportInterval=10 #report the results at intervals of this many iterations
runLength=3000000
output='outfile'
writeDiagnosticStats=0 #do we write out the value of each individual stat?

#### for flexible threshold simulations
flexiThreshold=1 # have an acceptance threshold that ratchets down slowly (1) or a fixed threshold (0)
Threshold=0 # the fixed initial threshold if flexiThreshold==0, or the variable one if flexiThreshold==1
finalThreshold=-1 # a target threshold to begin fixed-threshold simulations and recording.
ratchetUp=0.2 # when in the initial wandering stage, allow the threshold to drift up again
numberOfSimsPerAttempt=5 #simulate up to this many times to assess fit
#### for particle filter
nKernels=1 # no. kernels

#Dict of ABC parameters
ABCParms={'ranseed':ranseed, 'bigJump': bigJump,'littleJump':littleJump,'bigLittleRatio':bigLittleRatio, 'mutValues':mutValues,'burnIn':burnIn,'output':output,'flexiThreshold':flexiThreshold, 'Threshold':Threshold, 'finalThreshold': finalThreshold,'ratchetUp':ratchetUp,'nKernels':nKernels,'storeInterval':storeInterval, 'reportInterval':reportInterval, 'runLength':runLength, 'writeDiagnosticStats':writeDiagnosticStats,'numberOfSimsPerAttempt':numberOfSimsPerAttempt}

#simulation, fixed parameters
nReps=9 
nMales=2
nFemales=1
nPatches=2
nFood=1
FoodVal=1.0
Seconds=216000
nIntervals=100
maxMoves=480 #the most moves that are allowed in a simulation, 3x the max observed no.



#simulation, mutable parameters
fFoodPref=1.7691690118
mFoodPref=5.0588251127
fFemalePref=-17.505554338
mFemalePref=-9.6258394269
fMalePref=-10.6267615581
mMalePref=5.0288121643
fInertia=13.3345476504
mInertia=-2.6510616536
fLearn=4.7592925418
mLearn=0.567417047


#Dict of all simulation parameters
Parms={'fFoodPref' : fFoodPref, 'mFoodPref':mFoodPref,'fFemalePref':fFemalePref,'mFemalePref':mFemalePref,
         'fMalePref':fMalePref,'mMalePref':mMalePref,'fInertia':fInertia,'mInertia':mInertia,
         'fLearn':fLearn,'mLearn':mLearn,'nFemales':nFemales,'nMales':nMales,'nPatches':nPatches,'nFood':nFood,
         'FoodVal':FoodVal,'Seconds':Seconds, 'nIntervals':nIntervals,'maxMoves':maxMoves, 'nReps':nReps}
#Dict of mutable parameters, and their priors
ParmPriors={'fFoodPref' : [-20,20,40], 'mFoodPref':[-20,20,40],'fFemalePref':[-20,20,40],'mFemalePref':[-20,20,40],
         'fMalePref':[-20,20,40],'mMalePref':[-20,20,40],'fInertia':[-20,20,40],'mInertia':[-20,20,40],
         'fLearn':[0,10,10],'mLearn':[0,10,10]}

#Dicts of statistics, and their target values
moveStats={}
sampleStats={}
sampleStats['regPropFoodF']=[regPropFoodF,[1.39623520737,0.921885766326,5.4952341365,1.36150445662]]
sampleStats['regPropFoodM']=[regPropFoodM,[1.05235259439,0.494100721436,5.09293376369,0.70344592017]]
sampleStats['regMPerF']=[regMPerF,[-0.363446022653,0.941859132309,15.6981818182,8.3742879801]]
sampleStats['regMPerM']=[regMPerM,[-0.41164535153,0.735345290689,29.7409090909,6.04639193006]]
moveStats['regDuration']=[regDuration,[-342.678659756,508.56862984,5073.15814197,5856.76606056]]
moveStats['regMovesF']=[regMovesF,[-0.133189407908,0.106269402771,1.88561145824,1.18974621611]]
moveStats['LikFoodF']=[LikFoodF,[1.00265461021,0.580253024874]]
moveStats['LikFoodM']=[LikFoodM,[0.88572521049,0.782432884337]]
moveStats['mPerF']=[mPerF,[1.45096238426,0.173145981592]]
moveStats['mPerM']=[mPerM,[-0.311141782407,0.118847017732]]
moveStats['regFLikLeaveM']=[regFLikLeaveM,[0.816264084391,0.633032065755,-1.54007950218,0.904601195498]]
moveStats['regMLikLeaveF']=[regMLikLeaveF,[-0.384309978512,0.382759847742,0.0103213101914,0.337915001826]]
moveStats['regMLikLeaveM']=[regMLikLeaveM,[-0.384309978512,0.382759847742,0.0103213101914,0.337915001826]]
moveStats['nMovesF']=[nMovesF,[82.125,39.3682533903]]
moveStats['nMovesM']=[nMovesM,[198.75,44.9353702555]]


#dicts are unordered, so I'll make an ordered vector for output: df = pd.DataFrame(data, columns=data.keys())
#df.to_csv('my_csv.csv', mode='a', header=False)
#These have extra parameters, not in the dicts themselves.
orderedOutColumns=['Iteration','Accepts','Threshold','fFoodPref','mFoodPref','fFemalePref','mFemalePref','fMalePref','mMalePref','fInertia','mInertia','fLearn','mLearn']

StatVecOut=['regPropFoodF_mean','regPropFoodF_slope','regPropFoodM_mean','regPropFoodM_slope',
'regMPerF_mean','regMPerF_slope','regMPerM_mean','regMPerM_slope','regDuration_mean',
'regDuration_slope','regMovesF_mean','regMovesF_slope',
'LikFoodF_mean','LikFoodM_mean','mPerF_mean','mPerM_mean','nMovesF_mean','nMovesM_mean',
'regFLikLeaveM_mean','regFLikLeaveM_slope','regMLikLeaveF_mean','regMLikLeaveF_slope',
'regMLikLeaveM_mean','regMLikLeaveM_slope','regDuration_slope','regMovesF_mean','regMovesF_slope']






